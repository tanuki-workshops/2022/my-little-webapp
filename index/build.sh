#!/bin/bash
export CI_PROJECT_ID="00000"
export CI_MERGE_REQUEST_IID="11111"
export CI_PROJECT_PATH="SOMEWHERE"
export CI_COMMIT_REF_SLUG="main"

tinygo build \
  -ldflags "-X main.ciProjectId=$CI_PROJECT_ID -X main.ciMergeRequestIid=$CI_MERGE_REQUEST_IID -X main.ciProjectPath=$CI_PROJECT_PATH -X main.ciCommitRefSlug=$CI_COMMIT_REF_SLUG"" \
  -o tinygo.index.wasm -target wasm ./main.go
ls -lh *.wasm

